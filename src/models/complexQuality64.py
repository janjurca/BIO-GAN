import torch.nn as nn

nz = 64
ngf = 64
ndf = 42
nc = 1


class Generator(nn.Module):
    def __init__(self):
        super(Generator, self).__init__()

        self.prep = nn.Sequential(
            nn.Linear(nz, 128),
            nn.Tanh()
        )

        self.main = nn.Sequential(
            nn.ConvTranspose2d(     128, ngf * 16, 4, 1, 0),
            nn.BatchNorm2d(ngf * 16),
            nn.ReLU(True),
            # state size. (ngf*16) x 4 x 4
            nn.ConvTranspose2d(ngf * 16, ngf * 8, 4, 2, 1),
            nn.BatchNorm2d(ngf * 8),
            nn.ReLU(True),
            # state size. (ngf*8) x 8 x 8
            nn.ConvTranspose2d(ngf * 8, ngf * 4, 4, 2, 1),
            nn.BatchNorm2d(ngf * 4),
            nn.ReLU(True),
            # state size. (ngf*4) x 16 x 16
            nn.ConvTranspose2d(ngf * 4, ngf * 4, 4, 2, 1),
            nn.BatchNorm2d(ngf * 4),
            nn.ReLU(True),

            nn.ConvTranspose2d(ngf * 4, ngf * 2, 4, 2, 1),
            nn.BatchNorm2d(ngf * 2),
            nn.ReLU(True),

            nn.Conv2d(ngf * 2, ngf * 2, 4, 1, 2),
            nn.BatchNorm2d(ngf * 2),
            nn.ReLU(True),

            # state size. (ngf*2) x 32 x 32
            nn.ConvTranspose2d(ngf * 2, ngf, 4, 2, 1),
            nn.BatchNorm2d(ngf),
            nn.ReLU(True),

            nn.Conv2d(ngf, ngf, 4, 1, 2),
            nn.BatchNorm2d(ngf),
            nn.ReLU(True),

            nn.Conv2d(ngf, ngf, 4, 1, 2),
            nn.BatchNorm2d(ngf),
            nn.ReLU(True),


            # state size. (ngf) x 64 x 64
            nn.ConvTranspose2d(ngf, nc, 3, 1, 1),
            nn.Tanh(),
            # state size. (nc) x 128 x 128
        )

    def forward(self, input):
        output = self.prep(input)
        output = output.unsqueeze(2).unsqueeze(3)
        output = self.main(output)
        return output

    def nz_size(self):
        return nz
