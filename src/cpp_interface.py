import subprocess
from PIL import Image
import os
from ctypes import cdll, c_int, c_char_p
import time
import subprocess

from os import listdir
from os.path import isfile, join
import glob
import numpy as np

try:
    lib = cdll.LoadLibrary('../utils/cpp_interface.so')
except Exception as  e:
    print("Error while loading library, do you have LD_LIBRARY_PATH configured?")
    raise(e)

class Cpp_interface(object):
    def __init__(self):
        self.obj = lib.Interface_new()

    def __del__(self):
        if os.path.exists("tmp1.tiff"):
            os.remove("tmp1.tiff")
        if os.path.exists("tmp2.tiff"):
            os.remove("tmp2.tiff")

    def compare(self, fp1, fp2):
        return lib.Interface_compare(self.obj, c_char_p(fp1.encode('UTF-8')), c_char_p(fp2.encode('UTF-8')))

    def compare_tifs(self, fp1, fp2):
        return lib.Interface_compare_tifs(self.obj, c_char_p(fp1.encode('UTF-8')), c_char_p(fp2.encode('UTF-8')))

    def create_markants(self, fp, fp2=None):
        if fp.endswith(".tif") and fp2==None:
            fp2 = fp[:-3] + "markants"
        if fp2 == None:
            raise ValueError('Input is not tif and output name not specified. Dont know what to do...')

        lib.Interface_create_markants(self.obj, c_char_p(fp.encode('UTF-8')), c_char_p(fp2.encode('UTF-8')))

    def compare_batch(self, b1, b2):
        scores = []
        for (im1, im2) in zip(b1, b2):
            score = self.compare_pils(im1, im2)
            scores.append(score)
        return scores

    def compate_batch_with_single(self, batch, image):
        scores = []
        for sample in batch:
            score = self.compare_pils(sample, image)
            scores.append(score)
        return scores


    def preprocess_dataset(self, path_to_dataset="../Dataset"):
        files = [os.path.join(path_to_dataset, f) for f in os.listdir(path_to_dataset) if os.path.isfile(os.path.join(path_to_dataset, f)) and f.endswith(".tif")]
        for f in files:
            self.create_markants(f)
            print("Markants for file: " + str(f) + " created.")
        print("Processed " + str(len(files)) + "files.")

    def self_test(self):
        self.create_markants("../Dataset/012_3_3.tif")
        self.create_markants("../Dataset/012_3_4.tif")
        score = self.compare("../Dataset/012_3_3.markants", "../Dataset/012_3_3.markants")

        if score == 808:
            print("Self test ok.")
        else:
            print("Something went wrong :(.")

    def compare_pils(self, im1, im2):
        im1.resize((504,480)).save("tmp1.tiff",format="TIFF", compression="tif_lzw", dpi=(504, 480))
        im2.resize((504,480)).save("tmp2.tiff",format="TIFF", compression="tif_lzw", dpi=(504, 480))

        score = self.compare_tifs("tmp1.tiff", "tmp2.tiff")
        return score

    def benchmark_compare(self):
        start = time.time()
        for _ in range(1000):
            self.compare_pils(Image.open("example/2.BMP"), Image.open("example/2.BMP"))

        end = time.time()
        print("Evaluation took " + str(end - start) + " seconds " + "Which is " + str((end - start) / 1000) + " per fp comparation.")

class Qualitator:

    def __init__(self, batch_size, path_to_binary="NFIQ2"):
        self.nfiq_path = path_to_binary
        #self.nfiq_path = "./../utils/NFIQ2/bin/NFIQ2"
        self.batch_size = batch_size
        self.create_in_csv()

    def __del__(self):
        files = ["in.csv"]
        for i in range(self.batch_size):
            files.append("tmp" + str(i) + ".bmp")

        for f in files:
            if os.path.exists(f):
                os.remove(f)

    def create_in_csv(self):
        with open("in.csv", "w") as file:
            for i in range(self.batch_size):
                file.write("tmp" + str(i) + ".bmp")
                file.write("\n")

    def get_quality(self, pil_image):
        pil_image.save("tmp0.bmp", dpi=(500, 500))
        command = [self.nfiq_path,"SINGLE", "tmp0.bmp", "BMP", "false", "false"]
        nfiq = subprocess.run(command, stdout=subprocess.PIPE)
        pre_score_text = "NFIQ2: Achieved quality score: "
        score = int(nfiq.stdout.decode("utf-8")[nfiq.stdout.decode("utf-8").find(pre_score_text) + len(pre_score_text)])

        return score

    def get_batch_quality(self, pil_array):
        for i, im in enumerate(pil_array):
            im.save("tmp" + str(i) + ".bmp", dpi=(500, 500))

        command = [self.nfiq_path, "BATCH", "in.csv", "BMP", "pes.csv", "false", "false"]
        nfiq = subprocess.run(command, stdout=subprocess.PIPE)
        scores = []
        for i in range(self.batch_size):
            pre_score_text = "tmp" + str(i) + ".bmp: NFIQ2 score = "
            score_position = str(nfiq.stdout.decode("utf-8")).find(pre_score_text)
            score_text = str(nfiq.stdout.decode("utf-8")).split("\n")[i + 3]

            score = int(score_text.strip().split()[-1])
            scores.append(score)

        return scores


if __name__ == "__main__":
    cpp = Cpp_interface()
    #cpp.self_test()
    #cpp.preprocess_dataset()
    #cpp.benchmark_compare()
    #cpp.compare_pils(Image.open("example/2.BMP"), Image.open("example/2.BMP"))


    images = []
    for img in glob.glob("Real/*"):
        image = Image.open(img).convert('L')
        images.append(image)

    q = Qualitator(len(images))
    batch_quality = q.get_batch_quality(images)
    #batch_quality = list(filter(lambda x: x < 10, batch_quality))
    batch_quality = list(filter(lambda x: x != 0, batch_quality))
    print(batch_quality)
    print("Mean", np.array(batch_quality).mean())
