import torch
import torchvision.transforms as transforms
import os


def tensorBatchToPilBatch(batch):
    res = []
    for i in range(batch.size(0)):
        res.append(transforms.ToPILImage()(batch[i]))
    return res


def saveTensorBatchImages(batch, dir):
    pils = tensorBatchToPilBatch(batch)
    savePilImages(pils, dir)


def savePilImages(pils, dir):
    if not os.path.exists(dir):
        os.makedirs(dir)
    for i, im in enumerate(pils):
        im = im.convert('L')
        im.save(dir.rstrip("/") + "/" + str(i) + ".bmp")
