import torch
import models.combination
import models.combinationMinimalistic
import models.combinationMinimalistic32
import models.combinationMinimalistic16
import models.complex
import models.complexQuality64
import models.simple
import torchvision.utils as vutils

import os
from utils import tensorBatchToPilBatch

dirname = os.path.dirname(__file__)
device = 'cuda' if torch.cuda.is_available() else 'cpu'


class Network:
    networks = {
        "combinationMinimalistic16": (models.combinationMinimalistic16, 'pths/netG_epoch_305_minimalistic16nz.pth'),
        "combinationMinimalistic32": (models.combinationMinimalistic32, 'pths/netG_epoch_369_combinationMinimalistic32.pth'),
        "combinationMinimalistic": (models.combinationMinimalistic, 'pths/netG_epoch_498_minimalistic_64nz.pth'),
        "complex": (models.complex, 'pths/netG_epoch_407_complex_64.pth'),
        "complexQuality": (models.complexQuality64, 'pths/netG_epoch_163_complex_with_quality_64.pth'),
        "combination": (models.combination, 'pths/netG_epoch_490_combination_augmented.pth'),
    }

    def __init__(self, net="combinationMinimalistic32"):

        net_selection = self.networks[net]
        self.net = net_selection[0].Generator().to(device)
        filename = os.path.join(dirname, net_selection[1])
        self.net.load_state_dict(torch.load(filename, map_location=torch.device(device)))

    def __call__(self, tensor):
        ret = []
        batch_size = tensor.size()[0]
        for i in range(0, batch_size, 32):
            # split large batch into smaller ones
            x = tensor[i:i+32]
            output = self.net(x.to(device))
            ret += tensorBatchToPilBatch(output.cpu())

        return ret

    def nz_size(self):
        return self.net.nz_size()

    @staticmethod
    def available():
        return list(Network.networks.keys())

    def gen_sample(self, folder):
        x = torch.randn(3, self.nz_size(), device=device)
        fake = self.net(x.to(device))
        vutils.save_image(fake.detach(), '%s/fake_samples.png' % (folder), normalize=True)
