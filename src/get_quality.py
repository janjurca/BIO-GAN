#!/usr/bin/env python

from cpp_interface import Qualitator
import argparse
from PIL import Image
import glob
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("--f1", type=str, default=None, help="finger1")
parser.add_argument("--folder", type=str, default=None, help="finger1")
parser.add_argument("--hist", type=str, default=None, help="Save histogram to file")
parser.add_argument("--title", type=str, default="", help="chart title")

args = parser.parse_args()

images = []

if args.f1:
    images.append((args.f1, Image.open(args.f1).convert('L')))

if args.folder:
    for img in glob.glob(args.folder + "/*"):
        image = Image.open(img).convert('L')
        images.append((img, image))


q = Qualitator(len(images))
batch_quality = q.get_batch_quality([x for y, x in images])

for quality, image in zip(batch_quality, images):
    print(image[0], quality)

#print(batch_quality)
#batch_quality = list(filter(lambda x: x < 10, batch_quality))
#batch_quality = list(filter(lambda x: x != 0, batch_quality))
print("Mean", np.array(batch_quality).mean())


if args.hist:
    import matplotlib.pyplot as plt
    plt.hist(batch_quality, range=(0, 100), bins=100)  # `density=False` would make counts
    plt.ylabel('Počet')
    plt.xlabel('NFIQ2 skóre')
    plt.yscale('log')
    plt.title(args.title)
    plt.savefig(args.hist)
