#!/usr/bin/env bash


cd $SCRATCHDIR || exit 1
git clone https://github.com/usnistgov/NFIQ2.git
cd NFIQ2/
module add numpy-1.11.1-py2.7.10
module add gcc-8.3.0
module add clang-9.0
module add cmake-2.8.12
mkdir libOpenCV && cd libOpenCV && cmake -D CMAKE_MAKE_PROGRAM=make ../OpenCV && make opencv_core opencv_ts opencv_imgproc opencv_highgui opencv_flann opencv_features2d opencv_calib3d opencv_ml opencv_video opencv_objdetect opencv_contrib opencv_nonfree opencv_gpu opencv_photo opencv_stitching opencv_videostab
cd $SCRATCHDIR || exit 1
cd NFIQ2/

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)/libOpenCV/lib/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)/biomdi/common/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)/biomdi/fingerminutia/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)/NFIQ2/NFIQ2Algorithm/lib/

make
export PATH=$PATH:$(pwd)/NFIQ2/NFIQ2Algorithm/bin
