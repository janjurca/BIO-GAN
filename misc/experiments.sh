set -x

python3.6 generate_images.py  --count 6000  --folder combinationMinimalistic16 --network combinationMinimalistic16
python3.6 generate_images.py  --count 6000  --folder combinationMinimalistic32 --network combinationMinimalistic32
python3.6 generate_images.py  --count 6000  --folder combinationMinimalistic --network combinationMinimalistic
python3.6 generate_images.py  --count 6000  --folder complex --network complex
python3.6 generate_images.py  --count 6000  --folder complexQuality --network complexQuality
python3.6 generate_images.py  --count 6000  --folder combination --network combination


python3.6 get_quality.py --folder combinationMinimalistic16 --title "NFIQ2 combinationMinimalistic16"  --hist combinationMinimalistic16.pdf > combinationMinimalistic16.out
python3.6 get_quality.py --folder combinationMinimalistic32 --title "NFIQ2 combinationMinimalistic32"  --hist combinationMinimalistic32.pdf > combinationMinimalistic32.out
python3.6 get_quality.py --folder combinationMinimalistic --title "NFIQ2 combinationMinimalistic"  --hist combinationMinimalistic.pdf > combinationMinimalistic.out
python3.6 get_quality.py --folder complex --title "NFIQ2 complex"  --hist complex.pdf > complex.out
python3.6 get_quality.py --folder complexQuality --title "NFIQ2 complexQuality"  --hist complexQuality.pdf > complexQuality.out
python3.6 get_quality.py --folder combination --title "NFIQ2 combination"  --hist combination.pdf > combination.out


python3.6 inference.py -s soft_tensor_mutation -g 250 -r 5 -a dataset_samples/155__M_Right_thumb_finger.BMP --network combinationMinimalistic32 >  155__M_Right_thumb_finger.BMP.out
python3.6 inference.py -s soft_tensor_mutation -g 250 -r 5 -a dataset_samples/198__M_Right_ring_finger.BMP --network combinationMinimalistic32 >  198__M_Right_ring_finger.BMP.out
python3.6 inference.py -s soft_tensor_mutation -g 250 -r 5 -a dataset_samples/21__M_Left_middle_finger.BMP --network combinationMinimalistic32 >  21__M_Left_middle_finger.BMP.out
python3.6 inference.py -s soft_tensor_mutation -g 250 -r 5 -a dataset_samples/301__F_Left_thumb_finger.BMP --network combinationMinimalistic32 >  301__F_Left_thumb_finger.BMP.out
python3.6 inference.py -s soft_tensor_mutation -g 250 -r 5 -a dataset_samples/34__M_Right_index_finger.BMP --network combinationMinimalistic32 >  34__M_Right_index_finger.BMP.out
