set -xe

python3 inference.py -s soft_tensor_mutation -a example/2.BMP -o 8 -b 128 -g 100 -r 10 -f 0.25 -n combinationMinimalistic32 >/storage/brno2/home/xkadle35/result_soft_tensor_mutation_f1.txt
python3 inference.py -s soft_tensor_mutation -a example/2.BMP -o 8 -b 128 -g 100 -r 10 -f 0.3 -n combinationMinimalistic32 >/storage/brno2/home/xkadle35/result_soft_tensor_mutation_f3.txt
python3 inference.py -s soft_tensor_mutation -a example/2.BMP -o 8 -b 128 -g 100 -r 10 -f 0.35 -n combinationMinimalistic32 >/storage/brno2/home/xkadle35/result_soft_tensor_mutation_f5.txt
python3 inference.py -s soft_tensor_mutation -a example/2.BMP -o 8 -b 128 -g 100 -r 10 -f 0.4 -n combinationMinimalistic32 >/storage/brno2/home/xkadle35/result_soft_tensor_mutation_f7.txt

