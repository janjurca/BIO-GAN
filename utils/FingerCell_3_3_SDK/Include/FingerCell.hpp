#ifndef FINGER_CELL_HPP_INCLUDED
#define FINGER_CELL_HPP_INCLUDED

#include <Core/NObject.hpp>
#include <Core/NTypes.hpp>
#include <Images/NImage.hpp>
namespace Neurotec {
namespace FingerCell {
using ::Neurotec::Images::HNImage;
#include <FingerCell.h>
}}

N_DEFINE_ENUM_TYPE_TRAITS(Neurotec::FingerCell, FingerCellTemplateFormat)

namespace Neurotec { namespace FingerCell
{

/// Provides functionality for fingerprints extraction and
/// matching in embedded devices.                         
class FingerCell : public NObject
{
	N_DECLARE_OBJECT_CLASS(FingerCell, NObject)
	N_DECLARE_MODULE_CLASS(FingerCell)

private:
	static HFingerCell Create()
	{
		HFingerCell handle;
		NCheck(FingerCellCreate(&handle));
		return handle;
	}

public:
	FingerCell ()
		: NObject(Create(), true)
	{
	}

	/// <summary>
	/// Extracts fingerprint features from an image.
	/// </summary>
	/// <param name="image">NImage object containing fingerprint
	///                     image.</param>
	/// <returns>
	/// NBuffer object - memory buffer containing extracted
	/// fingerprint features. 
	/// </returns>                                              
	::Neurotec::IO::NBuffer Extract(const ::Neurotec::Images::NImage & image) const
	{
		HNBuffer hValue;
		NCheck(FingerCellExtract(GetHandle(), image.GetHandle(), &hValue));
		return FromHandle< ::Neurotec::IO::NBuffer>(hValue);
	}

	/// <summary>
	/// Matches (1:1) two specified fingerprints. For 1:N scenario,
	/// this method can be used in a loop.
	/// </summary>
	/// <param name="reference">Memory buffer containing reference
	///                         fingerprint data.</param>
	/// <param name="candidate">Memory buffer containing candidate
	///                         fingerprint data. Candidate
	///                         fingerprint data will be compared
	///                         (matched) with reference data
	///                         (hReference).</param>
	/// <returns>
	/// Matching score (see Matching Threshold and FAR/FRR in
	/// documentation for details). The bigger the score is, more
	/// similar fingerprints are. 
	/// </returns>                                                 
	NInt Match(const ::Neurotec::IO::NBuffer & reference, const ::Neurotec::IO::NBuffer & candidate) const
	{
		NInt value;
		NCheck(FingerCellMatch(GetHandle(), reference.GetHandle(), candidate.GetHandle(), &value));
		return value;
	}

	/// <summary>
	/// Gets maximal number of records for merging
	/// </summary>  
	NInt MergeTemplatesGetMaxNumberOfRecords()
	{
		NInt count;
		FingerCellMergeTemplatesGetMaxNumberOfRecords(&count);
		return count;
	}

	/// <summary>
	/// Merge two or more templates into one.
	/// </summary>   
	::Neurotec::IO::NBuffer MergeTemplates(const ::Neurotec::NArrayWrapper< ::Neurotec::IO::NBuffer>& arrBuffers)
	{
		HNBuffer hValue;

		NCheck(FingerCellMergeTemplates(GetHandle(), const_cast<HNBuffer*>(arrBuffers.GetPtr()), arrBuffers.GetCount(), &hValue));
		return FromHandle< ::Neurotec::IO::NBuffer>(hValue);
	}

	/// <summary>
	/// Gets minimal extracted template quality value which is
	/// considered as acceptable. If quality is lower, template is
	/// not extracted. Range: [0, 100]. Default value: 60. 
	/// </summary>                                                
	NInt GetQualityThreshold() const
	{
		return GetProperty<NInt>(N_T("QualityThreshold"));
	}

	/// <summary>
	/// Sets minimal extracted template quality value which is
	/// considered as acceptable. If quality is lower, template is
	/// not extracted. Range: [0, 100]. Default value: 60. 
	/// </summary>                                                
	void SetQualityThreshold(NInt value)
	{
		SetProperty(N_T("QualityThreshold"), value);
	}

	/// <summary>
	/// <paraattr align="justify">
	/// Gets matching algorithm to test a different version. Default
	/// algorithm is optimal for most cases. Use only when
	/// recommended by Neurotechnology for some specific scenario.
	/// Range: [0, 100]. Default value: 0. 
	/// </paraattr>
	/// </summary>                                                  
	NInt GetMatchingAlgorithm() const
	{
		return GetProperty<NInt>(N_T("MatchingAlgorithm"));
	}

	/// <summary>
	/// <paraattr align="justify">
	/// Sets matching algorithm to test a different version. Default
	/// algorithm is optimal for most cases. Use only when
	/// recommended by Neurotechnology for some specific scenario.
	/// Range: [0, 100]. Default value: 0.
	/// </paraattr>
	/// </summary>
	/// <param name="value">Matching algorithm version. </param>    
	void SetMatchingAlgorithm(NInt value)
	{
		SetProperty(N_T("MatchingAlgorithm"), value);
	}

	/// <summary>
	/// Gets FingerCell template format.
	/// </summary>                      
	FingerCellTemplateFormat GetTemplateFormat() const
	{
		return GetProperty<FingerCellTemplateFormat>(N_T("TemplateFormat"));
	}

	/// <summary>
	/// Sets FingerCell template format value.
	/// </summary>
	/// <param name="value">FingerCell template format value to set.</param>
	void SetTemplateFormat(FingerCellTemplateFormat value)
	{
		SetProperty(N_T("TemplateFormat"), value);
	}
};

}}

#endif // !FINGER_CELL_HPP_INCLUDED
