/*! \file FingerCell.h
	\brief Fingercell API definitions.
*/
#ifndef FINGER_CELL_H_INCLUDED
#define FINGER_CELL_H_INCLUDED

#include <Core/NObject.h>
#include <Images/NImage.h>

#ifdef N_CPP
extern "C" {
#endif

N_DECLARE_OBJECT_TYPE(FingerCell, NObject)
N_DECLARE_MODULE(FingerCell)

typedef enum FingerCellTemplateFormat_
{
	fctfProprietary = 0,
	fctfIso = 1,
	fctfMoc = 2,
	fctfUnknown = 3,
} FingerCellTemplateFormat;

N_DECLARE_TYPE(FingerCellTemplateFormat)

/**
 * \brief Create FingerCell object.
 * 
 * \param[out] phFingerCell pointer to #HFingerCell.
 * 
 * \return  If the function succeeds the return value is #N_OK.
 * \return  If the function fails the return value is error code.
 * 
 */
NResult N_API FingerCellCreate(HFingerCell * phFingerCell);

/**
 * \brief Return maximal number of records for merging
 *
  * \param[out] pCount maximal number of records for merging
  */

void N_API FingerCellMergeTemplatesGetMaxNumberOfRecords(NInt * pCount);

/**
 * \brief Merge templates into single template
 *
 * \param[in] hFingerCell Handle of FingerCell object.
 * \param[in] phSourceTemplates Pointer to template handles.
 * \param[in] templateCount Templates count.
 * \param[out] phSingleTemplate Pointer to template handle for output.
 *
 * \return  If the function succeeds the return value is #N_OK. phSingleTemplate is filled with merged template data.
 * \return  If the function fails the return value is error code.
 *
 */

NResult N_API FingerCellMergeTemplates(HFingerCell hFingerCell, HNBuffer * phSourceTemplates, NInt templateCount, HNBuffer * phSingleTemplate);

/**
 * \brief Extract minutiae data from finger print image
 *
 * \param[in] hFingerCell Handle of FingerCell object.
 * \param[in] hImage Handle of finger print image.
 * \param[out] phRecord Pointer to #HNBuffer.
 *
 * \return  If the function succeeds the return value is #N_OK. phRecord is filled with minutiae data.
 * \return  If the function fails the return value is error code.
 *
 */

NResult N_API FingerCellExtract(HFingerCell hFingerCell, HNImage hImage, HNBuffer * phTemplate);

/**
 * \brief Match two fingerprint records.
 * 
 * \param[in] hFingerCell Handle of FingerCell object.
 * \param[in] hReference Handle of reference record.
 * \param[in] hCandidate Handle of candidate record.
 * \param[out] pScore pointer to #NInt.
 * 
 * \return  If the function succeeds the return value is #N_OK. pScore is filled with matching score.
 * \return  If the function fails the return value is error code.
 * 
 */
NResult N_API FingerCellMatch(HFingerCell hFingerCell, HNBuffer hReference, HNBuffer hCandidate, NInt * pScore);

#ifdef N_CPP
}
#endif

#endif // !FINGER_CELL_H_INCLUDED
