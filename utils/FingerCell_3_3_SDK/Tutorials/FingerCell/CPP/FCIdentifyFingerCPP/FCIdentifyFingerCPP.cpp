#include <TutorialUtils.hpp>

#include <string>

#include <NCore.hpp>
#include <FingerCell.hpp>
#include <NMedia.hpp>
#include <NLicensing.hpp>
//#include <chrono> 
//using namespace std::chrono; 

using namespace std;
using namespace Neurotec;
using namespace Neurotec::Licensing;
using namespace Neurotec::IO;
using namespace Neurotec::Images;

const NChar title[] = N_T("FCIdentifyFingerCPP");
const NChar description[] = N_T("Demonstrates fingerprint identification.");
const NChar version[] = N_T("3.3.0.0");
const NChar copyright[] = N_T("Copyright (C) 2018-2020 Neurotechnology");


class Interface{
    public:
        const NChar * license; 
        Interface(){
	        license = { N_T("FingerCell") };
	        NLicenseManager::SetTrialMode(1);

	        if (!NLicense::Obtain(N_T("/local"), N_T("5000"), license))
	        {
	        	NThrowException(NString::Format(N_T("Could not obtain licenses: {S}"), license));
	        }

        }
        ~Interface(){
	        OnExit();
        }

        int compare(char* ffp, char* sfp){
            try
            {
	             ::Neurotec::FingerCell::FingerCell fingerCell;
                 std::string ffps(ffp);
                 std::string sfps(sfp);

	             NBuffer probeRecord = NFile::ReadAllBytes(ffps);
	             NBuffer galleryRecord = NFile::ReadAllBytes(sfps);
	             int score = (int)fingerCell.Match(probeRecord, galleryRecord);
                 return score;
            }
            catch (NError& ex)
        	{
                cout << "Error when comparing. File FCIdentifyFingerCPP.cpp";
		        LastError(ex);
                return -1;
	        }
        }

        void create_markants(char* input, char* output){
            try
            {
                ::Neurotec::FingerCell::FingerCell fingerCell;
		        NImage image = NImage::FromFile(input);
		        NBuffer record = fingerCell.Extract(image);
		        NFile::WriteAllBytes(output, record);
            }
	        catch (NError& ex)
	        {
                cout << "Error while creating markants. File FCIdentifyFingerCPP.cpp";
		        LastError(ex);
	        }
        }

        int compare_tifs(char* input1, char* input2){
            try
            {
                ::Neurotec::FingerCell::FingerCell fingerCell;

		        NImage image1 = NImage::FromFile(input1);
		        NImage image2 = NImage::FromFile(input2);


		        NBuffer record1 = fingerCell.Extract(image1);
		        NBuffer record2 = fingerCell.Extract(image2);

	            int score = (int)fingerCell.Match(record1, record2);

                return score;
            }
	        catch (NError& ex)
	        {
		        //LastError(ex);
                //cout << "Error when comparing TIFs. File FCIdentifyFinger.cpp";
                return -1;
	        }

        }
};

extern "C" {
    Interface* Interface_new(){ return new Interface(); }
    int Interface_compare(Interface* c, char* fp1, char* fp2){
        return (c->compare(fp1, fp2)); 
    }
    int Interface_compare_tifs(Interface*c, char* fp1, char* fp2){
        return (c->compare_tifs(fp1, fp2));
    }
    void Interface_create_markants(Interface* c, char* fp1, char* fp2){
        return (c->create_markants(fp1, fp2));
    }
}


