#! /bin/bash
HERE=$(dirname $(readlink -f $0))

#Create sample binary

cd $HERE/FingerCell_3_3_SDK/Tutorials/FingerCell/CPP/FCIdentifyFingerCPP

g++ -fPIC -I../../../../Tutorials/Common/C/ -I../../../../Tutorials/Common/CPP/ -Wall  -fno-strict-aliasing -O2 -DNDEBUG -I../../../../Include/ -D__LINUX__   -c FCIdentifyFingerCPP.cpp -o .obj/release/Linux_x86_64/FCIdentifyFingerCPP/FCIdentifyFingerCPP.o



g++ -shared  -Wl,-soname,comparer.so -O3 -L../../../../Lib/Linux_x86_64/ -Wl,-z,muldefs -Wl,-rpath=\${ORIGIN}/../../Lib/Linux_x86_64/ -Wl,-rpath=../../../../Lib/Linux_x86_64/,-rpath-link=../../../../Lib/Linux_x86_64/    .obj/release/Linux_x86_64/FCIdentifyFingerCPP/FCIdentifyFingerCPP.o -Wl,--start-group -lFingerCell -lNMedia -lNCore -lNLicensing    -lpthread -ldl   -lm -Wl,--end-group -o .obj/release/Linux_x86_64/FCIdentifyFingerCPP/comparer.so


cp -f $HERE/FingerCell_3_3_SDK/Tutorials/FingerCell/CPP/FCIdentifyFingerCPP/.obj/release/Linux_x86_64/FCIdentifyFingerCPP/comparer.so $HERE/cpp_interface.so

echo "Please put following line in your bashrc"
echo "export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:$HERE/FingerCell_3_3_SDK/Lib/Linux_x86_64"
