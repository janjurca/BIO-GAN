# Projekt BIO - Generování otisků prstů pomocí soupeřících sítí a odezvy porovnávacího systému

---
author:
- Jan Jurča (xjurca08), Josef Kadleček (xkadle35)
---
# Pro nejlepší zážitek čtěte dokumentaci:
[dokumentace.pdf](https://gitlab.com/kohoutovice/BIO-GAN/-/blob/master/dokumentace.pdf)
---
# Zadání

Cílem projektu bylo vytvořit model, případně algoritmus, který bude schopen určitým postupem, nazvěme jej inference, zrekonstruovat neznámý, utajený, otisk. Tento utajený otisk je vložený v systému a naše jediná možná interakce je určení podobnosti s jiným, námi předloženým, otiskem.

V ideálním případě by tedy systém měl být schopen vygenerovat co nejpodobnější otisk otisku utajenému.

# Navržená řešení

Jako návrh řešení, byly zvoleny generativní soupeřící neuronové sítě,
neboli *GAN*. Návrh byl následující: natrénujeme neuronovou síť, která
bude schopna generovat otisky prstu. Vstupem generativní sítě bude
latentní vektor, podle kterého síť bude daný otisk generovat.

Při návrhu jsme vycházeli z myšlenky, že pokud jsou dva latetní vektory
v prostoru blízko sebe, jsou i na jejich základě vygenerované otisky
podobné. Narozdíl od situace, kdy máme dva vzdálené latentní vektory,
pak i vygenerované otisky budou odlišné.

Na tomto základě jsme se rozhodli síť nátrénovanou pro generování
všemožných otisků a následně využít pro inferenci utajeného otisku. Tuto
inferenci jsme se rozhodli zrealizovat po vzoru genetických algoritmů,
kde evoluovanými daty je právě latentní vektor a jako chybová funkce nám
poslouží podobností skóre vůči utajenému otisku.

## Využité externí nástroje

Při řešení jsme využili dva nástroje, které pracují s otisky prstů.
Prvním z nich je *VeryFinger* od firmy Neurotechnology[^1]. Tento
nástroj jsme využili pro porovnávání otisků prstů, respektive jsme tímto
nástrojem určovali skóre shody dvou otisků.

Po prozkoumání dokumentace jsme autory uvedené minimální skóre 200,
které už lze v určitých situacích považovat za shodu, přijali jako
cílovou hodnotu, kterou je cílem překročit.

Dále jsme, pro výpočet kvality otisku, využili doporučený nástroj
NFIQ2[^2].

# Generování otisků pomocí GAN

Jak již bylo nastíněno v návrhu řešení. Rozhodli jsme se natrénovat
neuronovou síť schopnou generovat obecný otisk prstu. Využili jsme
technologie soupeřících neuronových sítí GAN, která je založena na
trénování dvou sítí současně. První síť je zde generátor, který na
základě latentního vektoru vygeneruje obrázek a druhá síť,
diskriminátor, který rozhoduje zda-li se jedná o vygenerovaný obrázek,
nebo o reálný příklad z datasetu.

Proces trénování je tedy souboj dvou sítí, které na počátku trénování
generují spíše šum než reálné výsledky, ale trénováním se obě sítě
zlepšují. Tento proces lze vysvětlit tak, že generátor se snaží ošálit
diskriminátor aby vygenerovaný obrázek klasifikoval jako reálný.
Diskriminátor se ovšem postupně zlepšuje a je ve svých klasifikacích
přesnější, proto i generátor musí generovat kvalitnější výstupy, aby
obelstil diskriminátor.

Vstupem generátoru je latentní vektor. Latentní vektor se při trénovaní
generuje náhodně z normálního rozložení. Tento předpoklad o normálním
rozložení, vychází z očekávání, že jednotlivé vzorky v datasetu budou
také, určitým způsobem reprezentovat normální rozložení. Tedy, že
většina vzorků si bude jakýmsi způsobem podobná a velké odlišnosti budou
spíše výjimkou.

## Datová sada

K trénování jsme využili datovou sadu SOCOFing[^3]. Jedná se o datovou
sadu obsahující 6000 originálních otisků a dalších více než 17000
augmentovaných otisků, simulujících poškození.

Pro účely našeho trénování jsme využili pouze množinu originálních
otisků, protože jsme usoudili, že není zcela žádoucí trénovat na
algoritmicky poškozených otiscích. Místo deformovaných otisků z
datasetu, jsme se rozhodli pro vlastní augmentaci dat, která sestávala z
poměrně méně destruktivních operací. Jedná se o operace náhodné rotace,
horizontálního a vertikálního překlopení. V jiných případech je často
využívána i oprace náhodného zvětšení, kterou jsme se rozhodli nepoužít,
protože by jsme v mnohých případech přišli o výraznou část otisku.

## Architektury

Existuje mnoho různých architektur, které se v této oblasti využívají.
Od minimalistických sítí složených pouze z jedné, či více plně
propojených prstev až po složité reziduální sítě, případně přístupy
používané v sítích typu StyleGAN[^4].

My jsme se rozhodli vydat střední cestou komplexity a využili jsme
architektury DCGAN, tedy generativní hluboké konvoluční sítě.

**Generátor.** Jedna ze dvou sítí je právě generátor. Oproti klasické
konvoluční síti se liší především použitou operací, kterážto je
transponovaná konvoluce.

![transpconv](/images/transpconv.png)


Díky transponované konvoluci je možné s každou další vrstvou získat
obrázek o vyšším rozlišení. Toho je samozřejmě využito. Vstupní latentní
vektor, délky $n$ je interpretován jako obrázek o jednom pixelu a $n$
kanálech. Průchodem síťí se transformuje, postupně se snižuje počet
kanálů a zvyšuje rozlišení.

Při řešení jsme natrénovali několik různých sítí u kterých jsme mírně
měnili strukturu vrstev a optimalizovali hyperparametry. Protože se
jednotlivé architetury neliší výrazně a pro celkovou stručnost jsou
architektury uvedeny v tabulce.

Konkrétně se jedná o tři architektury,
které jsme interně pojmenovali simple, combination a complex, kde simple
využívá pouze transponovaných 2D konvolucí a combiantion a complex
využívá i tradičních 2D konvolucí. Přístup využití i tradiční konvoluce
pro určitou chytrou filtraci se projevil pozitivně na výsledcích.
Především jasnějším ohraničením otisků a nižším šumem jak v okolí otisku
tak i v jeho ploše.

  |Architektura generátoru
  |-------------------------------------------------------------|
  |Linear(in=latent_len, out=128)|
  |TConv2D(ic=128, oc=ngf\*16, k=4, s=1, p=0)|
  |TConv2D(ic=ngf\*16, oc=ngf\*8, k=4, s=2, p=1)|
  |TConv2D(ic=ngf\*8, oc=ngf\*4, k=4, s=2, p=1)|
  |TConv2D(ic=ngf\*4, oc=ngf\*4, k=4, s=2, p=1)|
  |TConv2D(ic=ngf\*4, oc=ngf\*2, k=4, s=2, p=1)|
  |Conv2d(ic=ngf\*2, oc=ngf\*2, k=4, s=1, p=2)|
  |TConv2D(ic=ngf\*2, oc=ngf, k=4, s=2, p=1)|
  |Conv2d(ic=ngf, oc=ngf, k=4, s=1, p=2)|
  |Conv2d(ic=ngf, oc=ngf, k=4, s=1, p=2)|
  |TConv2D(ic=ngf, oc=1, k=3, s=1, p=1)|

  : Souhrná tabulka zobrazující všechny tři použité architektury
  generátoru. Proměnná ngf zde zastupuje laditelný hyperparametr, který
  jsme ve většině případů měli nastavený na $64$. V tabulce dále nejsou
  vypsány aktivační funkce a batchnormalizace. Celkový výpis postihuje
  architekturu *complex*. Pokud odstraníme předposlendí vrstvu
  vytvoříme architelturu *combination* a vynecháme-li všechny tradiční
  konvoluce, získáme architekturu *simple*.

**Diskriminátor.** Realizuje binární klasifikaci vstupního obrázku do
tříd *fake* a *real*. Samotný diskriminátor je čistě klasifikační síť, v
našem případě sestávající z několika kovolučních vrstev. Pro úplnost je
námi využitá architektura klasifikátoru uvedena v tabulce

  |Architektura diskriminátoru |
  |-------------------------------------------------|
  |Conv2d(inc=1, outc=ndf, k=4, s=3, p=1)|
  |Conv2d(inc=ndf, outc=ndf\*4, k=3, s=2, p=1)|
  |Conv2d(inc=ndf\*4, outc=ndf\*8, k=3, s=2, p=1)|
  |Conv2d(inc=ndf\*8, outc=ndf\*16, k=4, s=3, p=1)|
  |Conv2d(inc=ndf\*16, outc=1, k=4, s=2, p=0)|

  : Architektura diskriminátoru uvedená bez aktivačních funkcí. Proměnná
  ndf je laditelný hyperparametr kapacity sítě, který jsme podle typu
  generátoru nastavovali od $32$ do $64$.
:::

**Diskriminátor s vyžitím NFIQ2.** V rámci splnění covid dodatku zadání,
jsme natrénovali i síť, kde při trénovaní byl diskriminátoru ku pomoci
NFIQ2. Klasifikace byla tedy ovlivněna i vypočítanou kvalitou. Toto
řešení jsme neshledali užitečným ani nápomocným. Celkově to zpomalilo
proces trénování a kvalitu nenavýšilo. Důvody výsledeku tohoto trénování
jsou rozebrány v sekci Vyhodnoceni.

## Implementace a trénování

K samotné implementaci sítě jsme vyžili framework PyTorch[^5].
Zreprodukovat naše trénovaní je možné pomocí skriptu train.py,
respektive train_with_quality.py, který automaticky stáhne dataset a
započne trénovaní. Dále se mezi odevzdanými soubory nachází několik
dalších skriptů pro snadné generování obrázků, určení kvality a dalších
potřebné nástroje. Natrénované sítě lze nalézt v našem repozitáři[^6].

Trénování tohoto typu sítí je časově velmi náročné a nejinak tomu je i v
našem případě. Počet epoch se u nás pohyboval kolem 400 až 500 epoch,
což odpovídá přibližně 10 hodinám na grafické kartě nvidia 1080TI.

## Vyhodnocení natrénovaných sítí {#sec:vyhodnoceni-gany}

Protože jsme natrénovali hned několik sítí s různými parametry je
žádoucí je navzájem porovnat vzhledem ke kvalitě generovaných otisků. K
tomuto vyhodnocení jsme využili právě NFIQ2. Budeme porovnávat průměrnou
hodnotu kvality 6000 náhodně vygenerovaných otisků z jednotlivých sítí.
Tyto hodnoty také porovnáme vůči průměrné hodnotě kvality otisků z
datasetu. Všechny tyto hodnoty jsou uvedeny v tabulce.

|**Data** |  **NFIQ2 skóre**|
|--------------------|------------------------|
|SOCOfing |      3.64|
|combination lat=16 |      2.74|
|combination lat=32 |      2.32|
|combination lat=64 |      2.82|
|combination lat=256 |       2.7|
|complex lat=64 |      2.72|
|complex with NFIQ2 lat=64 |      2.83|

  : Souhrn výsledků průměrných kvalit otisků. Proměnná lat definuje
  použitou délku latentního vektoru.
:::

Jak je z tabulky vidět. Všechny natrénované sítě dosáhly lepšího
průměrného NFIQ2 skóre. Tento výsledek nás překvapil, ale i znejistil,
jestli jsme výpočet provedli správně. Po překontrolování se nám výsledky
potrvdily a došli jsme ke dvěma důvodům, jak je možné, že data ze sítě
si na tom stojí lépe než vzorová data.

Prvním argumentem je to, že síť pravděpodobně negeneruje moc obskurní
otisky, které se v datasetu mohou vyskytovat. Generuje spíše průměrně
vypadající otisky, bez výrazných výchylek a deformací.

Druhým argumentem je samotný nástroj NFIQ2, jehož vyhodnocení kvality se
zdá býti v určitých situacích pochybné. Jako ilustrace je zde uveden
obrázek, který přes svoji mizivou podobnost s
otiskem dostává od NFIQ2 hodnocení skóre $2$, tedy poměrně slušnou
kvalitu:

![NFIQ ](/images/nfiq_good.jpg)




Přestože jsme právě naznačili pochybnou kvalitu výsledků z NFIQ2. Budeme
se jeho výsledků držet i nadále a pro úplnost zde uvedeme ještě dva
histogramy kvality otisků z datasetu a otisků generovaných pomocí sítě
*combination lat=32*.

Histogram kvality reálných otisků:
![NFIQ ](/images/real.png)

---

Histogram kvality vygenerovaných otisků:
![NFIQ ](/images/comb.png)


Na samém závěru této sekce ještě ukážeme 3 náhodně vygenerované otisky z
dané sítě:

![NFIQ ](/images/fake_samples_.png)

# Inference

Poté co byla natrénována první poyužitelná neruonová síť, započali práce
na vytváření heuristiky pro hledání ideálního latentního vektoru -
takového vstupu, jež na výstupu dává otisk co nejvíce shodný s
\"utajeným\" otiskem. Jak bylo již v návrhu řešení zmíněno, vycházeli
jsme z předpokladu, že pokud bude latentní vektor (dále jen vstup) lehce
pozměněn, bude lehce pozměněn i vygenerovaný otisk (dále jen výstup).
Tudíž jsme začali pracovat s myšlenkou využitích genetických algoritmů,
jejichž princip bude krátce nastíněn v následující sekci.

## Genetické algoritmy

Genetické (nebo též evoluční) alegoritmy, jsou jednou z metod
prohledávání prostoru. Zjednodušeně jde tato metoda shrnout do
následujících kroků:

1.  Vygeneruje se nová populace

2.  Populace se ohodnotí.

3.  Vyberou se noví jedinci, kteří budou rodiče.

4.  Jedinci se zmutují / zkříží, čímž vznikne nová populace.

5.  Pokud nebylo dosaženo kýženého výsledku, nebo nevypršel maximální
    počet generací, pokrčuj bodem dvě.

Jde o co nejgeneričtější popis, který lze aplikovat na většinu
genetických algoritmů. Specifický popis našeho užití je vysvětlen v
dalších sekcích.

## Heldání ideálního vstupu

V následujících sekcích bude popsána specifická aplikace metody
genetických algoritmů pro hledání latentního vektoru, který se promítne
na otisk, který je co nejpodobnejšího vůči \"utajenmému otisku\".

Pro určení skóre jedince používáme v úvodu zmíněný program třetí strany
*VeriFinger* a samotná evlouce (běh genetického algoritmu ) je
realizována programem inference.py, popsáním jeho argumentů si můžeme
prakticky anstínit jak je evoluce v našem případě realizována, tak
pojďme na to.

-   --batch_size je velikost populace (příhodně z důvodu optimalizace
    volena jako velikost batche), platí, že by měla být dělitelná počtem
    potomků (aby každý rodič měl stejný počet potomků). Výchozí je 128.

-   --offsprings je počet nových potomků každého rodiče. Výchozí je 8.

-   --mutation_probability u bodových mutací se jedná o pravděpdobnost,
    že daná položka vstupního vektoru bude změněna. Výchozí je 0.01, a
    jep onechána z dob kdy byl používán enormní latentní vektor
    (velikost 255).

-   --array_of_images jména souborů utajených otisků oddělených čárkou

-   --runs počet běhů pro každý utajený otisk

-   --network na které ze sítí probíhá vyhodnocování

-   --mutation_force jaký násobek normálního rozložení je k hodnotě
    vektoru přičten. Dle experiment se ukázala vhodná hodnota 0.25

-   --strategy strategie jak budou rodiče zmutování / zkříženi, aby
    vznikli noví jedinci.

V našem případě byla jako voba rodíčů použita metoda elitářství (vybírá
se batchsize / offsprings nejlepších jedinců) a tak asi nejhlavnějším
faktorem jsou implementované mutační strategie, a ty jsou následující:

-   random_search - tato metoda sloužáí pro porovnávání s náhodný
    porovnáváním a označit ji za genetický algoritmus je do jisté míry
    nadsázka

-   soft_random_mutation každý prvek latentního vektoru bude zmutován s
    pravděpodobností mutation_probability a silou mutation_force

-   random_mutation speciální případ soft_random_mutation,kdy síla je 1

-   block_mutation náhodný úsek latentního vektoru bude změněn

-   soft_tensor_mutation k celému latentnímu vektoru bude přičten
    náhodný velktor vynásobený mutation_force

## Experimenty GA + NN

V této chvíli máme vše potřebné k provedení experimentů inference
otisku. Z časových důvodů jsme provedli experiment inference 5 různých
otisků z datasetu. Pro každý otisk jsme provedli 5 běhů evoluce, aby
jsme získali určitou statistiku výsledků. Inference probíhala pomocí
sítě *combination lat=32*. Celková doba běhu experimentů se pohybovala
někde kolem 22 hodin. Výsledky inference jsou zobrazeny v grafu
    ![Box plot ](/images/boxplot.png)

Jak je z výsledků patrné výsledky nejsou moc přesvědčivé. Nutno je říci,
že se jedná o výsledky ze sítě s délkou vstupu 32. Tato síť nebyla
zvolena náhodně a primárně ani podle průměrné kvality otisku. Hlavním
argumentem pro volbu právě této sítě byla velikost prostoru nutného k
prohledání, která je oproti prostoru s délkou vstupu 256, s níž jsme
začínali, exponencálně menší.

Samozřejmě jsme experimentovali i s inferencí otisku, který byl
vygenerován sítí. Tím jsme měli zaručeno, že otisk lze danou sítí
opravdu vygenerovat. Výsledky těchto pokusů dopadali lépe, ale stále ne
ideálně. Proto jsme prozkoumali jak moc je síť citlivá na minimální
změnu hodnoty ve vstupu a zjistili jsme, že už přičtení hodnoty $0.1$ k
pouze jednomu prvku vektoru sníží skóre komparátoru na hodnoty kolem 50.
Tímto jsme si potvrdili, že je téměř nemožné najít daný otisk evolucí
vstupu.

Na popud těchto výsledků a zjištění jsme hledali další možný přístup k
řešení daného problému.

## Generování obrázku pomocí genetického algoritmu

Díky tomu že předchozí metoda nedosáhla uspokojivých výsledků a mimo
jiné i díky chybě ve výpočtu jednoho z řešitelů (která položila
předoklad, že stavový prostor bude v tomto případě řádově menší) vznikla
ještě metoda, kde genetický algoritmus pracuje přímo s maticí 100x100
jež reprezentuje obrázek vstupu do porovnávače. Genetický algoritmus
tedy mutuje přímo jednotlivé pixely vstupního obrázku a na základě
odezvy porovnávacího systému vybírá následující jedince.

Popis tohoto genetického předpokládá čtenářovu znalost z předchozích
kapitol, uvedeme zde tedy jen konkrétní změny:

-   populace je konstatní velikosti 100 a vybírá se 10 rodičů z nichž
    každý \"plodí\" 9 potomků a zachovává sebe

-   pro jednoduchost a demonstrační účely sám populaci inicializuje 100
    náhodnými otisky z datasetu

-   stejně tak udává jako cíl náhodnýotisk z datasetu, kterým se však
    neshoduje s otiskem inicializačním

-   algoritmus samovolně končí po dosažení skóre 250

## Experimenty GA

V rámci genetického algoritmu pracujícího přímo s maticí pixelů nebyl z
důvodu časové náročnosti proveden dostatečný počet experimentů, výsledky
jsou tudíž spíše orientační. Přesto je možné nad deseti běhy, nad deseti
různými otisky, pozorovat jistý trend. Zhruba třetina běhů dosáhla skóre
nad dvě sta (\"prolomila\" biometrii) a zároveň žádný běh nedosáhl skóre
menšího jednomu stu. Průměrné skóre z posledních šeti běhů při čtyřech
tisících generacích bylo 175. V nejlepším případě, byl genetický
algoritmus schopen dosáhnout skóre 331, následně vypršel alokovaný čas
na metacentru a běh byl ukončen. Hlavní nevýhodou tohoto přístupu je, že
výsledek je více podoben \"účinému šumu\", nežli otisku prstu - jistě by
tedy nebyl schopen ošálit člověka daktyloskopie znalého.


Vlevo - nalezený otisk pomocí přímého prohledávání stavového prostoru obrázku metodou genetických algoritmů. 
Vpravo - cílový (utajený) otisk. Skóre shody přesahuje hranici 200:
<img src="images/201.png" width="300" height="value">

## Závěr

Nyní, když byl čtenář obeznámen s celou problematikou a bylo mu
nastíněno řešení, je źahodno shrnout všechny přínosy tohoto projektu,
ukázat s jakými nečekanými uskálimi se při vypracovávání obdobných úloh
můžeme setakt a v neposlední řadě zmínit možnosti, kterými by šlo
pokračovat v načatých šlépějích projektu.

### Možnosti rozšíření

Práce ponechává dveře otevřené dalšímu bádání v pdoboě optimalizace
genetických algoritmmů, hojnějšího využití paralelizace (kterou metoda
genetických algoritmů nativně podporuje), implementace dalších a
sofistikovanějších způsobů mutací, výzkumu idálního rozlišení či případe
použití obrazových filtrů. Zároveň se jako reálný jeví hlubší způsob
prozkoumání funkce porovnávače a kvalitátoru, která se nejeví jako z
neintuitivnějších.

### Přínos

Neoddiskutovatelným přínosem tohoto projektu je generativní síť. Otisky
jí vygenrované lze bezostyšně označit za kvalitní (pro podložení tohoto
faktu se odkazujeme na program NFIQ2). Zároveň může čtenář posoudit
vizuální kvalitu vygenerovaných otisků, která je přinejmenším laickým
okem srovnatelná s profesionálními řešeními (alespoň do té míry, co se
nám podařilo nalézt). Co se prohledávání prostoru a zpětného nalezení
ideálního vstupu za účelem získání totožného otisku týče, spíše než
grandiózní úspěch můžeme tuto metodu označit za \"trnitou cestu k
cíli\". Skóre pohybující se nad hranicí čtiřiceti je sice řádově
uspokojivější, než porovnání s náhodným otiskem, avšak k přelstění
biometrických systému je jistě tuze daleko. Tento nezdar považují autoři
za vynahrazený metodou přímého prohledávání matice vstupu za pomoci
evolučního algoritmu, která v nadpoloviční většině případů dokázala
překonat zmiňovanou metu v podobě skóre rovného dvoum stům a ošálit
biometrický systém, za předpokladu, že odpovídá za pomoci skóre.

[^1]: <https://www.neurotechnology.com/fingercell.html>

[^2]: <https://github.com/usnistgov/NFIQ2>

[^3]: <https://core.ac.uk/download/pdf/161525096.pdf>

[^4]: <https://arxiv.org/pdf/1812.04948.pdf>

[^5]: https://pytorch.org/

[^6]: https://gitlab.com/kohoutovice/BIO-GAN


---
## Preparation
Have Dataset folder filled with data
Use ./compile to compile commercial tool for finger comparing score
Now you should be able to use enroll and identify

## Enrolling
You must first extract features by enrolling fingerprints
``` bash
./enroll ../Dataset/sample.tif ../Dataset_prepocessed/sample_processed
```

## Comparing fingerprints
``` bash
./identify processedSample1 processedSample2
``` 

